--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

nightsister_clothing_schem_pants = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "nightsister_pants_s01_schematic", weight = 5000000},
		{itemTemplate = "nightsister_pants_s02_schematic", weight = 5000000},
	}
}

addLootGroupTemplate("nightsister_clothing_schem_pants", nightsister_clothing_schem_pants)
