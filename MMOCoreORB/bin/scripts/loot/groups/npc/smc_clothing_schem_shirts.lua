--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

smc_clothing_schem_shirts = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "smc_shirt_s01_schematic", weight = 5000000},
		{itemTemplate = "smc_shirt_s02_schematic", weight = 5000000},
	}
}

addLootGroupTemplate("smc_clothing_schem_shirts", smc_clothing_schem_shirts)
