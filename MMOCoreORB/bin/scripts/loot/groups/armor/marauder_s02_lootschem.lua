marauder_s02_lootschem = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		{itemTemplate = "marauder_s02_chest_schematic", weight = 1250000},
		{itemTemplate = "marauder_s02_helm_schematic", weight = 1250000},
		{itemTemplate = "marauder_s02_leggings_schematic", weight = 1250000},
		--{itemTemplate = "marauder_s02_boots_schematic", weight = 1111111},
		{itemTemplate = "marauder_s02_gloves_schematic", weight = 1250000},
		{itemTemplate = "marauder_s02_bracer_l_schematic", weight = 1250000},
		{itemTemplate = "marauder_s02_bracer_r_schematic", weight = 1250000},
		{itemTemplate = "marauder_s02_bicep_l_schematic", weight = 1250000},
		{itemTemplate = "marauder_s02_bicep_r_schematic", weight = 1250000},
	}
}


addLootGroupTemplate("marauder_s02_lootschem", marauder_s02_lootschem)
