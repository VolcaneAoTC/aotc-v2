world_clonetrooper_captain_501 = Creature:new {
	objectName = "@mob/creature_names:clonetrooper_captain_501",
	mobType = MOB_NPC,
	randomNameType = NAME_SWAMPTROOPER,
	randomNameTag = true,
	socialGroup = "civilian",
	faction = "",
	level = 100,
	chanceHit = 1,
	damageMin = 600,
	damageMax = 610,
	baseXp = 3005,
	baseHAM = 39100,
	baseHAMmax = 49900,
	armor = 1,
	resists = {40,40,40,40,40,40,40,40,40},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + FACTIONAGGRO,
	diet = HERBIVORE,
	scale = 1.05,

	templates = {
		"object/mobile/dressed_clone_arc_501.iff",
		"object/mobile/dressed_clone_arc_nh_501.iff"
	},
	lootGroups = {

	},

	primaryWeapon = "clonetrooper_weapons_mix",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",
	reactionStf = "@npc_reaction/stormtrooper",
	personalityStf = "@hireling/hireling_stormtrooper",
	primaryAttacks = merge(riflemanmaster,carbineermaster,brawlermaster),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(world_clonetrooper_captain_501, "world_clonetrooper_captain_501")
