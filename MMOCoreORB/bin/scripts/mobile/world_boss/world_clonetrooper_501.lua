world_clonetrooper_501 = Creature:new {
	objectName = "@mob/creature_names:clonetrooper_501",
	mobType = MOB_NPC,
	randomNameType = NAME_SWAMPTROOPER,
	randomNameTag = true,
	socialGroup = "civilian",
	faction = "",
	level = 100,
	chanceHit = 1,
	damageMin = 340,
	damageMax = 450,
	baseXp = 2637,
	baseHAM = 29200,
	baseHAMmax = 30800,
	armor = 0,
	resists = {15,15,40,15,15,15,15,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + FACTIONAGGRO,
	diet = HERBIVORE,
	scale = 1.05,

	templates = {
		"object/mobile/dressed_clonetrooper_blue_m.iff"
	},
	lootGroups = {

	},

	primaryWeapon = "clonetrooper_weapons",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",
	reactionStf = "@npc_reaction/stormtrooper",
	personalityStf = "@hireling/hireling_stormtrooper",
	primaryAttacks = merge(riflemanmaster,carbineermaster,brawlermaster,marksmanmaster),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(world_clonetrooper_501, "world_clonetrooper_501")
