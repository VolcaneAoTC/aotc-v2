world_stormtrooper = Creature:new {
	objectName = "@mob/creature_names:storm_commando",
	mobType = MOB_NPC,
	randomNameType = NAME_STORMTROOPER,
	randomNameTag = true,
	socialGroup = "civilian",
	faction = "",
	level = 100,
	chanceHit = 1,
	damageMin = 740,
	damageMax = 750,
	baseXp = 2637,
	baseHAM = 29200,
	baseHAMmax = 33800,
	armor = 0,
	resists = {15,15,40,15,15,15,15,25,25},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + FACTIONAGGRO,
	diet = HERBIVORE,
	scale = 1.05,

	templates = {"object/mobile/dressed_stormtrooper_m.iff"},
	lootGroups = {

	},

	primaryWeapon = "stormtrooper_carbine",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",
	reactionStf = "@npc_reaction/stormtrooper",
	personalityStf = "@hireling/hireling_stormtrooper",
	primaryAttacks = merge(riflemanmaster,carbineermaster,brawlermaster,marksmanmaster),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(world_stormtrooper, "world_stormtrooper")
