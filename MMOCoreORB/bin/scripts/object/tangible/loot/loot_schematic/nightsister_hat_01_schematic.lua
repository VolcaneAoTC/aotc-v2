
object_tangible_loot_loot_schematic_nightsister_hat_01_schematic = object_tangible_loot_loot_schematic_shared_nightsister_hat_01_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_tailor_master",
	targetDraftSchematic = "object/draft_schematic/clothing/clothing_hat_nightsister_s01.iff",
	targetUseCount = 3
}

ObjectTemplates:addTemplate(object_tangible_loot_loot_schematic_nightsister_hat_01_schematic, "object/tangible/loot/loot_schematic/ns_hat_s01_schematic.iff")
