
object_tangible_loot_loot_schematic_smc_shirt_02_schematic = object_tangible_loot_loot_schematic_shared_smc_shirt_02_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_tailor_master",
	targetDraftSchematic = "object/draft_schematic/clothing/clothing_shirt_singing_mtn_02.iff",
	targetUseCount = 3
}

ObjectTemplates:addTemplate(object_tangible_loot_loot_schematic_smc_shirt_02_schematic, "object/tangible/loot/loot_schematic/smc_shirt_s02_schematic.iff")


